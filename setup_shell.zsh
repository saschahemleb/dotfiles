#!/usr/bin/env zsh
SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"

DOTFILES=(
  'profile'

  'git/gitignore'
  'git/gitconfig'
  'git/gitattributes'

  'vim/vimrc'

  'zsh/zshrc'

  'tmux/tmux.conf'
)

DOTFOLDERS=(
  'profile.d'
  'zsh/zsh.d'

  'tmux/tmux'
)

confirm() {
  printf "$1 [y/n] : "
  read -r opt
  echo
  [[ $opt =~ ^[Yy]$ ]]
  return
}


link() {
  target=$1
  source=$2

  if [ -e "${target}" ]; then
    if [ -L "${target}" ]; then
      # target is symlink, maybe we already linked it to source?
      real_target=`readlink ${target}`
      if [ "${real_target}" = "${source}" ]; then
        # skip this file because we already linked it
        return 0
      else
        echo "${real_target} is not the same as ${source}\n"
      fi
    fi
    if confirm "[?] Do you want to override ${target}?"; then
      rm "${target}"
    else
      return 1
    fi
  fi

  ln -s "$source" "$target"
}

for i in ${DOTFILES[@]}; do
  target="${HOME}/.${i##*/}"
  link $target "${SCRIPT_DIR}/${i}"
done

for i in ${DOTFOLDERS[@]}; do
  target="${HOME}/.${i##*/}"
  link $target "${SCRIPT_DIR}/${i}"
done
