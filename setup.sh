#!/usr/bin/env sh

set -e

SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"

echo "setup_brew"
"${SCRIPT_DIR}/setup_brew.sh"
echo "setup_shell"
"${SCRIPT_DIR}/setup_shell.zsh"
