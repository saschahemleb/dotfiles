function antibody_compile {
    antibody bundle < "${HOME}/.zsh.d/plugins.txt" > "${HOME}/.zsh.d/custom/_plugins.zsh"
    source "${HOME}/.zsh.d/custom/_plugins.zsh"
}

function antibody_edit_plugins {
    vim "${HOME}/.zsh.d/plugins.txt" && antibody_compile
}