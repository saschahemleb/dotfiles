#!/usr/bin/env sh

# iTerm2
# Fix 'Window resizing is off slightly'
defaults write com.googlecode.iterm2 DisableWindowSizeSnap -integer 1

# Rectangle
# Add gaps between windows
defaults write com.knollsoft.Rectangle gapSize -float 10
