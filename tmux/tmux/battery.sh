#!/usr/bin/env sh

percent=`pmset -g batt | grep -oE "\d+\%"`
total=`echo ${percent} | grep -oE "\d+"`
color="#[fg=colour2]"
if [[ "${total}" -lt "50" ]]; then
	color="#[fg=colour3]"
fi
if [[ "${total}" -lt "25" ]]; then
	color="#[fg=colour1]"
fi


echo "${color} ${percent}"
