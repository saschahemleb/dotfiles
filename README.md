# Dotfiles

## Installation

```
git clone ssh://git@git.hemleb.de:2222/saschahemleb/dotfiles.git ~/.dotfiles
~/.dotfiles/setup.sh
```

## Configuration

### iTerm2

1. Go to `Preferences -> Appearance -> Panes` And change both `Side margins` and `Top & bottom margins` to 20.

1. Go to `Profiles -> Default -> General` and change `Command` from `Login Shell` to `Command` and paste in `/usr/local/bin/tmux new -A -s main`

1. Go to `Profiles -> Default -> Colors` and under `Color Presets` make sure `Dracula+` and `ayu_light` are installed

1. Go to `Profiles -> Default -> Keys`, change `Left Option Key` and `Right Option Key` to `Esc+`. Change the key mappings for `option + <` and `option + >` to the escape sequences `b` and `f` respectively

1. Setup the `iterm2_auto_theme_switch.py` script (`Menu -> Scripts -> Manage -> Import...`)

