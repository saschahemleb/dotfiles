function print_colors {
    for i in {0..255}; do
        printf "\x1b[38;5;${i}mcolour${i}\x1b[0m\n"
    done
}

for profile ($HOME/.profile.d/*); do
    source $profile
done

[[ -f "$HOME/.profile.local" ]] && . "$HOME/.profile.local"
