set -e

SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"

if ! command -v brew &>/dev/null; then
    "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi

if ! brew bundle --file "${SCRIPT_DIR}/Brewfile" check &>/dev/null; then
    brew bundle --file "${SCRIPT_DIR}/Brewfile" install
fi