#!/usr/bin/env python3.7

import asyncio
import iterm2

async def main(connection):
    async with iterm2.VariableMonitor(connection, iterm2.VariableScopes.APP, "effectiveTheme", None) as mon:
        while True:
            theme = await mon.async_get()

            parts = theme.split(" ")
            if "dark" in parts:
                preset = await iterm2.ColorPreset.async_get(connection, "Dracula+")
            else:
                preset = await iterm2.ColorPreset.async_get(connection, "ayu_light")

            profiles = await iterm2.PartialProfile.async_query(connection)
            for partial in profiles:
                profile = await partial.async_get_full_profile()
                await profile.async_set_color_preset(preset)
                
iterm2.run_forever(main)
